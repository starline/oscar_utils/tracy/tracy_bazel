#include <unistd.h>

#include <iostream>
#include <memory>

#include "tracy/Tracy.hpp"

extern "C" void myloop(int count, double sleep_s) {
  std::cout << "entered runtime lib\n";
  for (int i = 0; i < count; ++i) {
    auto sleep_usec = std::make_shared<unsigned int>(
        sleep_s *
        1e6);  // use heap allocations to check if Tracy can track them
    ZoneScopedN("Runtime loop");
    usleep(*sleep_usec);
    std::cout << "cycles: " << i + 1 << "/" << count << "\n";
  }
}
