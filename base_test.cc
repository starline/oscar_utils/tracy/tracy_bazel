

#include <time.h>
#include <unistd.h>

#include <iostream>
#include <random>
#include <thread>
#include <vector>

#include "tracy/Tracy.hpp"

#if TRACY_ENABLE
void* operator new(std ::size_t count) {
  auto ptr = malloc(count);
  TracyAlloc(ptr, count);
  return ptr;
}
void operator delete(void* ptr) noexcept {
  TracyFree(ptr);
  free(ptr);
}

#ifdef TRACY_MANUAL_LIFETIME

struct Lifetime {
  Lifetime() { tracy::StartupProfiler(); }
  ~Lifetime() { tracy::ShutdownProfiler(); }
};

static Lifetime lt{};

#endif

#endif

int main() {
  ZoneScopedN("Main zone");
  char* dynamic_memory =
      new char[100];  // just to track if Tracy shows not deallocated objects

  std::cout << dynamic_memory << "\n";

  std::vector<std::thread> threads;

  std::mt19937 engine;
  std::normal_distribution<> normal_dist(10, 2);

  std::cout << "Starting 10 threads with random sleep() function around 10 "
               "seconds...\n";

  for (int i = 0; i < 10; i++) {
    threads.emplace_back([&, i, duration = normal_dist(engine)]() {
      tracy::SetThreadName(("thread " + std::to_string(i)).c_str());
      ZoneNamedNC(Producer, "thread zone", 0xFF7272, true);
      usleep(duration * 1e6);
    });
  }

  for (size_t i = 0, sz = threads.size(); i < sz; ++i) {
    threads[i].join();
    TracyPlot("Threads completed", static_cast<double>(i + 1));
  }
  delete[] dynamic_memory;

  std::cout << "Completed\n";
  return 0;
}
