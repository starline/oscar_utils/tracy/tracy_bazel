
#include <dlfcn.h>
#include <unistd.h>

#include <iostream>

#include "tracy/Tracy.hpp"

#ifdef TRACY_MANUAL_LIFETIME

struct Lifetime {
  Lifetime() {
    tracy::StartupProfiler();
    profiler_init = true;
  }
  ~Lifetime() {
    tracy::ShutdownProfiler();
    profiler_init = false;
  }
  std::atomic<bool> profiler_init;
};

static Lifetime lt{};

#endif

#if TRACY_ENABLE

void* operator new(std ::size_t count) {
  auto ptr = malloc(count);
  TracyAlloc(ptr, count);
  return ptr;
}

void operator delete(void* ptr) noexcept {
  TracyFree(ptr);
  free(ptr);
}
#endif

int main() {
  ZoneScopedN("Main zone");
  void (*loop)(int, double);

  auto flag = RTLD_LAZY;
  flag |= RTLD_GLOBAL;

  void* lib = dlopen(
      "/apollo/modules/oscar/tools/data/tracy_bazel/bazel-bin/runtimelib.so",
      flag);

  std::cout << "Library loaded\n";
  if (lib == nullptr) {
    std::cerr << dlerror() << "\n";
    return 0;
  }
  dlerror();

  std::cout << "Library loaded\n";
  void* lib_fn = dlsym(lib, "myloop");

  if (lib_fn == nullptr) {
    std::cerr << "Error accessing the symbol: " << dlerror() << "\n";
    return 0;
  }

  loop = reinterpret_cast<decltype(loop)>(lib_fn);

  int cycles = 3;
  double duration = 2;
  std::cout << "calling function\n";
  (*loop)(cycles, duration);

  dlclose(lib);
  std::cout << "Completed\n";

  return 0;
}
