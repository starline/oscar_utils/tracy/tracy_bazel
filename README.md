# Tracy with Bazel

This repo contains minimal example that demonstrates how to enable Tracy profiling in projects with Bazel build system.

## Usage

Requirements:
 - Bazel;
 - C++ compiler;
 - Compiled Capture tool from Tracy repo;
 - Compiled (Linux) or downloaded (Windows) Profiler tool from Tracy repo.


1. In terminal run capture tool:

```
./capture-release -f out.tracy
```
2. In 2nd terminal compile and run test program from this repository:
```
bazel run tracy_test
```
3. When program successfully exits and ```capture``` has written ```out.tracy```, you can explore it with Profiler tool. 

You should see somethind like this:
![](image.png)
